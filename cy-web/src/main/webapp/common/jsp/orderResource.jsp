<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script src="/statics/libs/jquery-1.7.2/jquery.min.js"></script>
<link rel="stylesheet" href="${rc.contextPath}/statics/css/font-awesome.min.css">
<!--layui ztree样式-->
<link rel="stylesheet" href="${rc.contextPath}/statics/plugins/layui/css/layui.css" media="all">
<script src="${rc.contextPath}/statics/plugins/layer/layer.js"></script>
<script src="${rc.contextPath}/statics/plugins/layui/layui.js"></script>
<script src="${rc.contextPath}/statics/plugins/ztree/jquery.ztree.all.min.js"></script>
<link rel="stylesheet" href="${rc.contextPath}/statics/plugins/ztree/css/metroStyle/metroStyle.css">
<!--js组件-->
<script src="${rc.contextPath}/common/js/pageGrid.js"></script>
<script src="${rc.contextPath}/common/js/selectTool.js"></script>
<script src="${rc.contextPath}/common/js/radioTool.js"></script>
<script src="${rc.contextPath}/common/js/checkboxTool.js"></script>
<script src="${rc.contextPath}/common/js/treeTool.js"></script>
<script src="${rc.contextPath}/common/js/labelTool.js"></script>
<script src="${rc.contextPath}/common/js/linkSelectTool.js"></script>
<script src="${rc.contextPath}/common/js/uploadTool.js"></script>
<script src="${rc.contextPath}/common/js/HuploadTool.js"></script>
<!--通用-->
<script src="${rc.contextPath}/common/js/whole/common.js"></script>
<script src="${rc.contextPath}/common/js/whole/setting.js"></script>
<script src="${rc.contextPath}/common/js/whole/utils.js"></script>
<!--其他-->
<link rel="stylesheet" href="${rc.contextPath}/common/css/cyType.css">
<link rel="stylesheet" href="${rc.contextPath}/common/css/yfdc.css">
<!--日期-->
<script src="${rc.contextPath}/statics/plugins/My97DatePicker/WdatePicker.js" type="text/javascript" charset="utf-8"></script>
<script src="${rc.contextPath}/statics/plugins/My97DatePicker/duceap.date.js" type="text/javascript" charset="utf-8"></script>
<!-- 必选，编辑表格样式 -->
<link rel="stylesheet" href="${rc.contextPath}/statics/plugins/editgrid/css/editGrid.css" type="text/css"/>
<!-- 可选，需要验证时加入 -->
<link rel="stylesheet" href="${rc.contextPath}/statics/plugins/editgrid/css/validationEngine.jquery.css" type="text/css"/>


<%--<script src="/statics/plugins/editgrid/plugin/jquery-1.7.2.js" type="text/javascript"></script>--%>
<!-- 可选，需要拖拽功能时加入 -->
<script src="${rc.contextPath}/statics/plugins/editgrid/plugin/jquery.dragsort-0.5.2.js" type="text/javascript" charset="utf-8"></script>
<!-- 可选，需要验证功能时加入 -->
<script src="${rc.contextPath}/statics/plugins/editgrid/plugin/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
<script src="${rc.contextPath}/statics/plugins/editgrid/plugin/jquery.validationEngine-zh_CN.js" type="text/javascript" charset="utf-8"></script>
<!-- 必选，编辑表格主函数 -->
<script src="${rc.contextPath}/statics/plugins/editgrid/js/yrm.editGrid.js" type="text/javascript" charset="utf-8"></script>
<!--上传-->
<script src="${rc.contextPath}/statics/plugins/Huploadify/jquery.Huploadify.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" href="${rc.contextPath}/statics/plugins/Huploadify/Huploadify.css">